public class Main {
    public static void main(String[] args) {
        // &&
        // & - lazy logical operator
        // ||
        // | - lazy logical operator
        // !
        // ^
        boolean b = returnTrue() || returnFalse();

    }

    public static boolean returnFalse() {
        System.out.println("Called false!");
        return false;
    }

    public static boolean returnTrue() {
        System.out.println("Called true!");
        return true;
    }
}
